#!/usr/bin/env bash
################################################
#
# Backup into Box.com script
#
# @author   YQ
# @email    yquan [at] msn [dot] com
# @reldate  2014-08-31
################################################
#  Copyright (c) 2010 YQ
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# ------------------ END OF LICENSE TEXT ------------------

usage()
{
cat << EOF
Usage: $0 -f /path/to/your/file [-b 1276439] [-s]

This script backup your file into remote box.com. File is splitted into pieces if it is over 150MB.

OPTIONS:
-h      Show this message
-f      Local full path file name to backup
-b      Box.com folder id (optional)
-s      whether split the file (more than 150MB) into pieces

EOF
}

set -e

SCRIPT_NAME=`basename $0`
SHELL_BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SHELL_BASE_DIR/box-backup.config
source $SHELL_BASE_DIR/box-backup.common
ARG_FILE_TO_BACKUP=
ARG_BOX_FOLDER_ID=$BOX_FOLDER_ID
ARG_AUTO_SPLIT=0

while getopts "hf:b:s" OPTION
do
case $OPTION in
    h)
        usage
        exit 1
    ;;
    f)
        ARG_FILE_TO_BACKUP=$OPTARG
    ;;
    b)
        ARG_BOX_FOLDER_ID=$OPTARG
    ;;
    s)
        ARG_AUTO_SPLIT=1
    ;;
    ?)
        usage
        exit
    ;;
esac
done

if [ -z "$ARG_FILE_TO_BACKUP" ]; then
    echo "Missing -f arguments!"
    usage
    exit
fi

# Build the runtime pid with the arguments
PID_FILE=/tmp/${SCRIPT_NAME}_${ARG_FILE_TO_BACKUP//\//_}_${ARG_BOX_FOLDER_ID//\//_}.pid
if [ -f $PID_FILE ]; then
    echo "Same process is running right now, exiting..."
    exit -1;
fi
# Ensure PID file is removed on program exit.
trap "rm -f -- '$PID_FILE'" EXIT
echo $$ > "$PID_FILE"

# Check if file to backup exisited
if [ ! -f $ARG_FILE_TO_BACKUP ]; then
    echo "File not found! Aborting..."
    exit -2;
fi

# If there is no folder id supplied with the command line, use the default one in .config instead
if [ ! -z $ARG_BOX_FOLDER_ID ]; then
    BOX_FOLDER_ID=$ARG_BOX_FOLDER_ID
    echo "Box folder ID given with command line, using it ($BOX_FOLDER_ID) for uploading."
fi

echo "- - - = = = BEGIN = = = - - -"

# Get the new box.com access token and store the new refresh token for next usage 
refresh_token=`cat $SHELL_BASE_DIR/refresh.token`
json=`curl $CURL_SOCKS_PROXY_ARG https://app.box.com/api/oauth2/token -d "grant_type=refresh_token&refresh_token=$refresh_token&client_id=$BOX_CLIENT_ID&client_secret=$BOX_CLIENT_SECRET" -X POST`
echo $json
prop='access_token'
access_token=`jsonval`
echo ACCESS TOKEN: $access_token

prop='refresh_token'
refresh_token=`jsonval`
echo REFRESH TOKEN: $refresh_token

echo $access_token > $SHELL_BASE_DIR/access.token
echo $refresh_token > $SHELL_BASE_DIR/refresh.token
chmod 0600 $SHELL_BASE_DIR/access.token
chmod 0600 $SHELL_BASE_DIR/refresh.token

# Split the file if the file size is more than 150MB
if [ "$OS_UNAME" = "Darwin" ]; then
    FILESIZE=$(stat -f "%z" "$ARG_FILE_TO_BACKUP")
else
    FILESIZE=$(stat -c%s "$ARG_FILE_TO_BACKUP")
fi

if [ $FILESIZE -gt 150000000 ] && [ $ARG_AUTO_SPLIT -eq 1 ]; then
    echo "File size: $FILESIZE (bigger than 150000000), preparing to split..."
    FILE_PATH="$(dirname $ARG_FILE_TO_BACKUP)"
    FILE_NAME="${ARG_FILE_TO_BACKUP##*/}"
    FILE_EXT="${ARG_FILE_TO_BACKUP##*.}"
    rm -rf $FILE_PATH/$FILE_NAME.tmp
    mkdir $FILE_PATH/$FILE_NAME.tmp
    echo file path: $FILE_PATH/$FILE_NAME.tmp
    echo file name: $FILE_NAME
    echo file ext: $FILE_EXT
    if [ "$FILE_EXT" = "$ARG_FILE_TO_BACKUP" ]; then
        # file has no ext
        echo "$ARG_FILE_TO_BACKUP has no file extension."
        FILE_EXT=""
    fi

    if [ "$OS_UNAME" = "Darwin" ]; then
        split -b 100m "$ARG_FILE_TO_BACKUP" "$FILE_PATH/$FILE_NAME.tmp/$FILE_NAME.part-"
    else
        split -d --verbose -b 100m "$ARG_FILE_TO_BACKUP" "$FILE_PATH/$FILE_NAME.tmp/$FILE_NAME.part-"
    fi
    echo "Use cat $FILE_NAME.part-* > $FILE_NAME to join the split." > $FILE_PATH/$FILE_NAME.tmp/$FILE_NAME.README.txt

    FILES=$FILE_PATH/$FILE_NAME.tmp/*
    for f in $FILES
        do
            echo "Processing file: $f..."
            curl $CURL_SOCKS_PROXY_ARG https://upload.box.com/api/2.0/files/content -H "Authorization: Bearer $access_token" -F filename=@$f -F folder_id=$BOX_FOLDER_ID
    done
    echo "Removing temporary files..."
    rm -rf $FILE_PATH/$FILE_NAME.tmp
    echo "Done removing."
else
    curl $CURL_SOCKS_PROXY_ARG https://upload.box.com/api/2.0/files/content --progress-bar -H "Authorization: Bearer $access_token" -F filename=@$ARG_FILE_TO_BACKUP -F folder_id=$BOX_FOLDER_ID
fi

echo ""
echo "- - - = = = END = = = - - -"
