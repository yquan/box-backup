#!/usr/bin/env bash
################################################
#
# Backup into Box.com initlialization script
#
# @author   YQ
# @email    yquan [at] msn [dot] com
# @reldate  2014-08-31
################################################
#  Copyright (c) 2010 YQ
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# ------------------ END OF LICENSE TEXT ------------------

set -e

SHELL_BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SHELL_BASE_DIR/box-backup.config
source $SHELL_BASE_DIR/box-backup.common

echo "- - - = = = BEGIN = = = - - -"
echo "Please browse to:"
echo "https://app.box.com/api/oauth2/authorize?response_type=code&client_id=$BOX_CLIENT_ID&state=security_token%3D$BOX_CLIENT_SECRET"
# Wait for user to input for the authorizatio code from browser
read -e -p "And enter the authorization code: " auth_code

json=`curl $CURL_SOCKS_PROXY_ARG https://app.box.com/api/oauth2/token -d "grant_type=authorization_code&code=$auth_code&client_id=$BOX_CLIENT_ID&client_secret=$BOX_CLIENT_SECRET" -X POST`
prop='access_token'
access_token=`jsonval`
echo $json
echo ""
echo ACCESS TOKEN: $access_token

prop='refresh_token'
refresh_token=`jsonval`
echo REFRESH TOKEN: $refresh_token

echo $access_token > $SHELL_BASE_DIR/access.token
echo $refresh_token > $SHELL_BASE_DIR/refresh.token
chmod 0600 $SHELL_BASE_DIR/access.token
chmod 0600 $SHELL_BASE_DIR/refresh.token

echo ""
echo "- - - = = = END = = = - - -"


