# Backup files into box.com

This project helps you to backup your files into your box.com, assuming you have unlimited (or big enough) storage space of your box.com account since I didn't check any storage exceed for the moment.

There are 2 major scripts that you shall be aware of:

* `box-backup-init.sh`: it's used for initializing your tokens (access token and refresh token), it is important that this shall be executed firstly at least once, unless `box-backup.sh` won't work properly without those token.
* `box-backup.sh`: main backup script that you would use on daily backup basis.


###Usage:

	Usage: ./box-backup.sh -f /path/to/your/file [-b 1276439]

	This script backup your file into remote box.com. File is splitted into pieces if it is over 150MB.

	OPTIONS:
	-h      Show this message
	-f      Local full path file name to backup
	-b      Box.com folder id (optional)



### Step 1: Deploy the script

You could download the latest version here: [https://bitbucket.org/yquan/box-backup/get/master.zip](https://bitbucket.org/yquan/box-backup/get/master.zip)

Unpack the files and put into your host which has the files to backup into box.com - It is recommended to run the backup as root user to avoid any permission problem, but it shall works fine with other users who have proper read and write right.

For example, you could create a directory under /root/ named `box-backup/`:

	mkdir /root/box-backup
	
check if any of your .sh file is not executable, remember to set them to be executable:

	chmod u+x /root/box-backup/*.sh

### Step 2: Change configuration

All shell scripts read the configuration from file `box-backup.config`, there is a sample configuration file is in place, named `box-backup.config.dist`.

Rename the file from `box-backup.config.dist` to `box-backup.config`, and replace the context as described below (you may need to create your own application at [http://developers.box.com](http://developers.box.com) in order to get the parameters):

* **BOX_CLIENT_ID**: your box application client id
* **BOX_CLIENT_SECRET**: your box application client secret
* **BOX_FOLDER_ID**: destination box folder id which you could normally find it from the box URL
* **CURL_SOCKS_PROXY**: proxy setting for connecting to box API, for example: "localhost:1080", leave it empty if you don't want to use any proxy

### Step 3: Initialize the token

Run box-backup-init.sh

	/root/box-backup-init.sh

It prompts you to browse to:

	https://app.box.com/api/oauth2/authorize?response_type=code&client_id=BOX_CLIENT_ID&state=security_token%3DBOX_CLIENT_SECRET

(the script replaces ***BOX_CLIENT_ID*** and ***BOX_CLIENT_SECRET*** with corresponding value as mentioned in step 2, and leave ***%3D*** as it is!)

Follow the screen on your browser with necessary user credential until the page redirects you to the url like 

	http://0.0.0.0/?state=security_token%3DqLn0Xq2q16Fx6SMVnPX6LvUrnagD6L4Z&code=tqcskQHLe1nWtPMKTDpd7eMM2o0Wsys6
	
Copy and paste the code (*tqcskQHLe1nWtPMKTDpd7eMM2o0Wsys6* in above example) into your terminal console when the script prompt you to finish the initialization.

### Step 4: Backup the file

Now Run following command will put the file `/path/to/file/backup.tar.gz` into the box folder you set:

	/root/box-backup.sh -f /path/to/file/backup.tar.gz


You can also configure the your crontab to make the script running regularly:

	*/5 * * * * /root/box-backup/box-backup.sh /path/to/file/backup.tar.gz
	
## Changelog

v1.5 on October 3rd, 2014

* box-backup.sh accept a new argument "-s", if this argument is given, automatic splitting for big files (more than 150MB), default is not splitting files into pieces
* minor bug fix

v1.4 on September 24th, 2014

* better command line parameter handling, usage now is: `./box-backup.sh -f /path/to/your/file [-b 1276439]`
* able to connect box api through socks proxy
 
v1.3 on September 5th, 2014

* Add pid file for mutex checking in order not to run the backup script for same file at the same time.

v1.2 on September 1st, 2014

* Maintenance release which contains a few minor fixes and improvements.

v1.1 on August 31, 2014

* Automatically split the files bigger than 100MB and upload them separately to smooth the big file transmit.

v1.0 on August 30, 2014

* Initial release

## TODOs

* Add more handling for upload failure and retry
* ~~Split the big files~~ (fixed in v1.1)
* Reduce the access token renewal upon each execution
* Create sub directory for each
* Check if the file to backup exists in the box.com, update the file through the update API if it exists and content is different.
* ~~mutex for not running the script for same file twice~~ (fixed in v1.3)
* ~~add option for being able to connect box api through socks proxy~~ (fixed in v1.4)
* ~~better command line parameter handling (add dash - getopts)~~ (fixed in v1.4)











